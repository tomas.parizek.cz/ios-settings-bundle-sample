//
//  ContentViewModel.swift
//  SettingsBundle
//
//  Created by Tomáš Pařízek on 27.11.2023.
//

import SwiftUI

class ContentViewModel: ObservableObject {

    @Published private(set) var state = ContentViewState()

    init() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(userDefaultsChanged),
            name: UserDefaults.didChangeNotification,
            object: nil
        )

        loadStateFromDefaults()
    }
}

private extension ContentViewModel {

    @objc func userDefaultsChanged() {
        loadStateFromDefaults()
    }

    func loadStateFromDefaults() {
        state = ContentViewState(
            name: UserDefaults.standard.string(forKey: "name_preference") ?? "",
            enabled: UserDefaults.standard.bool(forKey: "enabled_preference"),
            slider: UserDefaults.standard.double(forKey: "slider_preference")
        )
    }
}
