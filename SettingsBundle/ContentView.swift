//
//  ContentView.swift
//  SettingsBundle
//
//  Created by Tomáš Pařízek on 27.11.2023.
//

import SwiftUI

struct ContentView: View {

    @StateObject private var viewModel = ContentViewModel()

    var body: some View {
        VStack(alignment: .leading) {
            Text("Name: \(viewModel.state.name)")
            Text("Enabled: \(viewModel.state.enabled ? "Yes" : "No")")
            Text("Slider: \(viewModel.state.slider)")
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding()
    }
}

#Preview {
    ContentView()
}
