//
//  SettingsBundleApp.swift
//  SettingsBundle
//
//  Created by Tomáš Pařízek on 27.11.2023.
//

import SwiftUI

@main
struct SettingsBundleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
