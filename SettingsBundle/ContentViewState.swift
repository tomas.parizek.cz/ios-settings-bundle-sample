//
//  ContentViewState.swift
//  SettingsBundle
//
//  Created by Tomáš Pařízek on 27.11.2023.
//

import Foundation

struct ContentViewState {
    let name: String
    let enabled: Bool
    let slider: Double

    init(
        name: String = "",
        enabled: Bool = false,
        slider: Double = 0
    ) {
        self.name = name
        self.enabled = enabled
        self.slider = slider
    }
}
